<?php

namespace Scan\ButagazBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Fiche
 *
 * @ORM\Table(name="_fiche")
 * @ORM\Entity(repositoryClass="Scan\ButagazBundle\Entity\Repositories\FicheRepository")
 * @UniqueEntity(fields={"facture"} , message ="Numéro de facture déjà existant.")
 */
class Fiche
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="facture", type="string", length=255)
     */
    private $facture;

    /**
     * @var integer
     *
     * @ORM\Column(name="civilite", type="smallint")
     */
    private $civilite;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="text")
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="code_postal", type="string", length=8)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=255)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=20)
     */
    private $tel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_consignation", type="string", length=255)
     */
    private $dateConsignation;

    /**
     * @var string
     *
     * @ORM\Column(name="enseigne", type="string", length=255)
     */
    private $enseigne;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_deconsignation", type="string", length=255)
     */
    private $dateDeconsignation;

    /**
     * @var string
     *
     * @ORM\Column(name="numero_identite", type="string", length=255)
     */
    private $numeroIdentite;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_saisie", type="datetime")
     */
    private $dateSaisie;

      /**
      * @ORM\ManyToOne(targetEntity="Scan\ButagazBundle\Entity\User" , cascade={"persist"})
      */
    protected $user;

    public function __construct()
    {
        $this->setDateSaisie(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set facture
     *
     * @param string $facture
     * @return Fiche
     */
    public function setFacture($facture)
    {
        $this->facture = $facture;
    
        return $this;
    }

    /**
     * Get facture
     *
     * @return string 
     */
    public function getFacture()
    {
        return $this->facture;
    }

    /**
     * Set civilite
     *
     * @param integer $civilite
     * @return Fiche
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;
    
        return $this;
    }

    /**
     * Get civilite
     *
     * @return integer 
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Fiche
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Fiche
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Fiche
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     * @return Fiche
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
    
        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string 
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     * @return Fiche
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return string 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return Fiche
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    
        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    

    /**
     * Set enseigne
     *
     * @param string $enseigne
     * @return Fiche
     */
    public function setEnseigne($enseigne)
    {
        $this->enseigne = $enseigne;
    
        return $this;
    }

    /**
     * Get enseigne
     *
     * @return string 
     */
    public function getEnseigne()
    {
        return $this->enseigne;
    }

    

    /**
     * Set numeroIdentite
     *
     * @param string $numeroIdentite
     * @return Fiche
     */
    public function setNumeroIdentite($numeroIdentite)
    {
        $this->numeroIdentite = $numeroIdentite;
    
        return $this;
    }

    /**
     * Get numeroIdentite
     *
     * @return string 
     */
    public function getNumeroIdentite()
    {
        return $this->numeroIdentite;
    }

    /**
     * Set dateSaisie
     *
     * @param \DateTime $dateSaisie
     * @return Fiche
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;
    
        return $this;
    }

    /**
     * Get dateSaisie
     *
     * @return \DateTime 
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * Set user
     *
     * @param \Scan\ButagazBundle\Entity\User $user
     * @return Fiche
     */
    public function setUser(\Scan\ButagazBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Scan\ButagazBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}