<?php

namespace Scan\ButagazBundle\Services;


/**
* 
*/
class TraitementFiche
{
	
	function __construct($oContainer)
	{
		$this->oSession = $oContainer->get("session");
		$this->oRequest = $oContainer->get("request");
		$this->oManager = $oContainer->get("doctrine")->getManager();
		$this->sIP 		= $this->oRequest->server->get("REMOTE_ADDR");
	}

	private $oManager;
	private $oRequest;
	private $oSession;


	public function fetchFile()
	{
		if ( $this->checkIfExistsInInstance() )
		{
			$aFiles = scandir($this->getFullPath());
			$this->putFileInProcess($aFiles[2]);
			$this->instanceInSession($aFiles[2]);
			return $this->getStaticPath("Process") . '/' . $aFiles[2];
		}

		return false;
	}

	public function instanceInSession($sFile)
	{
		$this->oSession->set('Fiche', $sFile);
		$this->oSession->set('IP', $this->sIP);
		return true;
	}

	public function checkifExists($iContract)
	{
		return (file_exists($this->getFullPath("Archived") . '/' . $iContract . '.jpg')) ? $this->getStaticPath("Archived") . '/' . $iContract . '.jpg' : false;
	}

	public function putFileInProcess($sFile)
	{
		rename(
				$this->getFullPath("Instance") . '/' . $sFile,
				$this->getFullPath("Process") . '/' . $sFile
			);

		return true;
	}

	public function endProcess($sFile, $iFacture)
	{
		$this->putFileInArchived($sFile, $iFacture);
		$this->oSession->remove('Fiche');
		$this->oSession->remove('IP');
		return true;
	}

	public function putFileInArchived($sFile, $iFacture)
	{
		rename(
				$this->getFullPath("Process") . '/' . $sFile,
				$this->getFullPath("Archived") . '/' . $iFacture . '.' . pathinfo($sFile, PATHINFO_EXTENSION)
			);

		return true;
	}

	public function checkIfExistsInInstance()
	{
		return ( count(scandir($this->getFullPath())) >= 3 ) ? true : false;
	}

	public function getFullPath($sType = "Instance")
	{
		//return $this->oRequest->getBasePath() . '/Fiches/' . $sType;
		return $this->oRequest->server->get('DOCUMENT_ROOT') . substr($this->oRequest->getBasePath(), 1) . '/Fiches/' . $sType;
	}

	public function getStaticPath($sType = "Instance")
	{
		return $this->oRequest->getBasePath() . '/Fiches/' . $sType;
	}

	public function fetchFileProcessed($sFile)
	{
		return $this->oRequest->getBasePath() . '/Fiches/Process/' . $sFile;
	}

}




 ?>