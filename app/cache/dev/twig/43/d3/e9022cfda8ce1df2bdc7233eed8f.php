<?php

/* ScanButagazBundle:Authentification:connexion.html.twig */
class __TwigTemplate_43d3e9022cfda8ce1df2bdc7233eed8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ScanButagazBundle::LayoutScanBundle.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ScanButagazBundle::LayoutScanBundle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Page de d'acceuil
";
    }

    // line 7
    public function block_header($context, array $blocks = array())
    {
        // line 8
        echo " \t";
        $this->displayParentBlock("header", $context, $blocks);
        echo "
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "\t<div class=\"container-fluid wrapper body\" >
\t\t<div class=\"logo\">
\t\t\t<img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/img/logo.gif"), "html", null, true);
        echo "\" title=\"Neo-Data\"/>
\t\t\t<h2>Authentification</h2>
\t\t</div>
\t\t<form class=\"form-horizontal\" role=\"form\" id=\"connexion-form\" method=\"post\" autocomplete=\"off\" action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("scan_butagaz_login_check"), "html", null, true);
        echo "\" >
\t\t\t";
        // line 18
        if ($this->getContext($context, "erreur")) {
            // line 19
            echo "\t\t\t    <div class=\"alert alert-danger\">
\t\t\t    \t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t<p>Oops ! un erreur est survenu ,<br /> veuillez bien saisir votre nom d'utilisateur et mot de passe .</p>
\t\t\t\t</div>
\t\t\t";
        }
        // line 24
        echo "\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"inputEmail1\" class=\"col-lg-2 control-label\">Utilisateur</label>
\t\t\t\t<div class=\"col-lg-10\">
\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"inputEmail1\" name=\"_username\" placeholder=\"Utilisateur\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getContext($context, "derniere_username"), "html", null, true);
        echo "\" />
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"inputPassword1\" class=\"col-lg-2 control-label\">Mot de passe</label>
\t\t\t\t<div class=\"col-lg-10\">
\t\t\t\t\t<input type=\"password\" class=\"form-control\" name=\"_password\" id=\"inputPassword1\" placeholder=\"Mot de passe\">
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"col-lg-offset-2 col-lg-10\">
\t\t\t\t\t<button type=\"submit\" class=\"btn btn-default\">Valider</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</form>
\t</div>
";
    }

    // line 45
    public function block_footer($context, array $blocks = array())
    {
        // line 46
        echo "\t";
        $this->displayParentBlock("footer", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "ScanButagazBundle:Authentification:connexion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 46,  103 => 45,  82 => 27,  77 => 24,  70 => 19,  68 => 18,  64 => 17,  58 => 14,  54 => 12,  51 => 11,  44 => 8,  41 => 7,  34 => 4,  31 => 3,);
    }
}
