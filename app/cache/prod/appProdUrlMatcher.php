<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);

        // scan_butagaz_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'scan_butagaz_homepage');
            }

            return array (  '_controller' => 'Scan\\ButagazBundle\\Controller\\DefaultController::indexAction',  '_route' => 'scan_butagaz_homepage',);
        }

        // scan_butagaz_connexion
        if ($pathinfo === '/connexion') {
            return array (  '_controller' => 'Scan\\ButagazBundle\\Controller\\AuthentificationController::connexionAction',  '_route' => 'scan_butagaz_connexion',);
        }

        // scan_butagaz_login_check
        if ($pathinfo === '/login_check') {
            return array('_route' => 'scan_butagaz_login_check');
        }

        // scan_butagaz_logout
        if ($pathinfo === '/deconnexion') {
            return array('_route' => 'scan_butagaz_logout');
        }

        if (0 === strpos($pathinfo, '/inscription')) {
            // scan_butagaz_inscription
            if ($pathinfo === '/inscription') {
                return array (  '_controller' => 'Scan\\ButagazBundle\\Controller\\AuthentificationController::inscriptionAction',  '_route' => 'scan_butagaz_inscription',);
            }

            // scan_butagaz_fichek
            if ($pathinfo === '/inscription') {
                return array (  '_controller' => 'Scan\\ButagazBundle\\Controller\\AuthentificationController::inscriptionAction',  '_route' => 'scan_butagaz_fichek',);
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
