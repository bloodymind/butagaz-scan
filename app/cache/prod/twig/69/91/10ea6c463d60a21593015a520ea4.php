<?php

/* ScanButagazBundle:Default:index.html.twig */
class __TwigTemplate_699110ea6c463d60a21593015a520ea4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ScanButagazBundle::LayoutScanBundle.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ScanButagazBundle::LayoutScanBundle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Page de d'acceuil
";
    }

    // line 7
    public function block_header($context, array $blocks = array())
    {
        // line 8
        echo " \t";
        $this->displayParentBlock("header", $context, $blocks);
        echo "
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "<div class=\"row-fluid saisie_fiche\">
\t<div class=\"span6\" >
\t\t<div class=\"container-fluid wrapper body\" >
\t\t\t<div class=\"logo\">
\t\t\t\t<img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/img/logo.gif"), "html", null, true);
        echo "\" title=\"Neo-Data\"/>
\t\t\t\t<h2>Inscription</h2>
\t\t\t</div>
\t  \t\t<form id=\"inscription\" class=\"form-signin\" method=\"post\" autocomplete=\"off\" accept-charset=\"utf-8\" ";
        // line 19
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'enctype');
        echo ">
\t\t  \t\t";
        // line 20
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "session"), "flashbag"), "get", array(0 => "inscription_terminer"), "method")) {
            // line 21
            echo "\t\t\t\t    <div class=\"alert alert-success\">
\t\t\t\t    \t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t\t<p>Félicitation ,<br /> Votre compte a été créer.</p>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        // line 26
        echo "\t\t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 27
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "facture"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Contrat :"));
        echo "
\t  \t\t\t\t";
        // line 28
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "facture"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 29
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "facture"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t                ";
        // line 32
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "civilite"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Civilité* :"));
        echo "
\t                ";
        // line 33
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "civilite"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 34
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "civilite"), 'errors');
        echo "
\t            </div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 37
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "nom"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Nom* :"));
        echo "
\t  \t\t\t\t";
        // line 38
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "nom"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 39
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "nom"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 42
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "prenom"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Prénom* :"));
        echo "
\t  \t\t\t\t";
        // line 43
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "prenom"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 44
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "prenom"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 47
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "adresse"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Adresse :"));
        echo "
\t  \t\t\t\t";
        // line 48
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "adresse"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 49
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "adresse"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 52
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "codePostal"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Code postal :"));
        echo "
\t  \t\t\t\t";
        // line 53
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "codePostal"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 54
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "codePostal"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 57
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "tel"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Tél :"));
        echo "
\t  \t\t\t\t";
        // line 58
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "tel"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 59
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "tel"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 62
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "ville"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Ville :"));
        echo "
\t  \t\t\t\t";
        // line 63
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "ville"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 64
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "ville"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 67
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "dateConsignation"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Date de consignation* :"));
        echo "
\t  \t\t\t\t";
        // line 68
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "dateConsignation"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 69
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "dateConsignation"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 72
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "dateDeconsignation"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Date de déconsignation* :"));
        echo "
\t  \t\t\t\t";
        // line 73
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "dateDeconsignation"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 74
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "dateDeconsignation"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 77
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "enseigne"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Enseigne* :"));
        echo "
\t  \t\t\t\t";
        // line 78
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "enseigne"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 79
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "enseigne"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 82
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "numeroIdentite"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Numéro d'identité* :"));
        echo "
\t  \t\t\t\t";
        // line 83
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "numeroIdentite"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 84
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($_form_, "numeroIdentite"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t";
        // line 86
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($_form_, 'rest');
        echo "
\t\t\t\t<button type=\"submit\" class=\"btn btn-default\">Valider</button>
\t\t\t</form>
\t\t</div>
\t</div>
\t<div class=\"span6\">
\t\tklfjdklfjdmklfjkdf
\t</div>
</div>
";
    }

    // line 97
    public function block_footer($context, array $blocks = array())
    {
        // line 98
        echo "\t";
        $this->displayParentBlock("footer", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "ScanButagazBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  305 => 98,  302 => 97,  287 => 86,  281 => 84,  276 => 83,  271 => 82,  264 => 79,  259 => 78,  254 => 77,  247 => 74,  242 => 73,  237 => 72,  230 => 69,  225 => 68,  220 => 67,  213 => 64,  208 => 63,  203 => 62,  196 => 59,  191 => 58,  186 => 57,  179 => 54,  174 => 53,  169 => 52,  162 => 49,  157 => 48,  152 => 47,  145 => 44,  140 => 43,  135 => 42,  128 => 39,  123 => 38,  118 => 37,  111 => 34,  106 => 33,  101 => 32,  94 => 29,  89 => 28,  84 => 27,  81 => 26,  74 => 21,  71 => 20,  66 => 19,  60 => 16,  54 => 12,  51 => 11,  44 => 8,  41 => 7,  34 => 4,  31 => 3,);
    }
}
