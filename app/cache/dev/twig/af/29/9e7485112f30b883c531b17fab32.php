<?php

/* ScanButagazBundle:Authentification:inscription.html.twig */
class __TwigTemplate_af299e7485112f30b883c531b17fab32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ScanButagazBundle::LayoutScanBundle.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ScanButagazBundle::LayoutScanBundle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Page de d'acceuil
";
    }

    // line 7
    public function block_header($context, array $blocks = array())
    {
        // line 8
        echo " \t";
        $this->displayParentBlock("header", $context, $blocks);
        echo "
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "\t<div class=\"container-fluid wrapper body\" >
\t\t<div class=\"logo\">
\t\t\t<img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/img/logo.gif"), "html", null, true);
        echo "\" title=\"Neo-Data\"/>
\t\t\t<h2>Inscription</h2>
\t\t</div>
\t  \t\t<form id=\"inscription\" class=\"form-signin\" method=\"post\" autocomplete=\"off\" accept-charset=\"utf-8\" ";
        // line 17
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'enctype');
        echo ">
\t\t  \t\t";
        // line 18
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "inscription_terminer"), "method")) {
            // line 19
            echo "\t\t\t\t    <div class=\"alert alert-success\">
\t\t\t\t    \t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t\t<p>Félicitation ,<br /> Votre compte a été créer.</p>
\t\t\t\t\t</div>
\t\t\t\t";
        }
        // line 24
        echo "\t  \t\t\t<div class=\"item\">
                    ";
        // line 25
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "civilite"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Civilité* :"));
        echo "
                    ";
        // line 26
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "civilite"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
                    ";
        // line 27
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "civilite"), 'errors');
        echo "
                </div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "nom"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Nom* :"));
        echo "
\t  \t\t\t\t";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "nom"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 32
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "nom"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "prenom"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Prénom* :"));
        echo "
\t  \t\t\t\t";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "prenom"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 37
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "prenom"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "adresse"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Adresse :"));
        echo "
\t  \t\t\t\t";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "adresse"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 42
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "adresse"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "tel"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Tél :"));
        echo "
\t  \t\t\t\t";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "tel"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 47
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "tel"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "code_postal"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Code postal :"));
        echo "
\t  \t\t\t\t";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "code_postal"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 52
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "code_postal"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "email"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "E-mail* :"));
        echo "
\t  \t\t\t\t";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "email"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 57
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "email"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "username"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Nom d'Utilisateur* :"));
        echo "
\t  \t\t\t\t";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "username"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 62
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "username"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "password"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Mot de passe* :"));
        echo "
\t  \t\t\t\t";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "password"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 67
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "password"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t<div class=\"item\">
\t  \t\t\t\t";
        // line 70
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "roles"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Role* :"));
        echo "
\t  \t\t\t\t";
        // line 71
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "roles"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t                ";
        // line 72
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "roles"), 'errors');
        echo "
\t  \t\t\t</div>
\t  \t\t\t";
        // line 74
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'rest');
        echo "
\t\t\t\t<button type=\"submit\" class=\"btn btn-default\">Valider</button>
\t\t\t</form>
\t</div>
";
    }

    // line 80
    public function block_footer($context, array $blocks = array())
    {
        // line 81
        echo "\t";
        $this->displayParentBlock("footer", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "ScanButagazBundle:Authentification:inscription.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 81,  228 => 80,  219 => 74,  214 => 72,  210 => 71,  206 => 70,  200 => 67,  196 => 66,  192 => 65,  186 => 62,  182 => 61,  178 => 60,  172 => 57,  168 => 56,  164 => 55,  158 => 52,  154 => 51,  150 => 50,  144 => 47,  140 => 46,  136 => 45,  130 => 42,  126 => 41,  122 => 40,  116 => 37,  112 => 36,  108 => 35,  102 => 32,  98 => 31,  94 => 30,  88 => 27,  84 => 26,  80 => 25,  77 => 24,  70 => 19,  68 => 18,  64 => 17,  58 => 14,  54 => 12,  51 => 11,  44 => 8,  41 => 7,  34 => 4,  31 => 3,);
    }
}
