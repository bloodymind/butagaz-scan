<?php

namespace Scan\ButagazBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Scan\ButagazBundle\Entity\Fiche;
use Scan\ButagazBundle\Form\FicheType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $oSession = $this->get('session');
        $oRequest = $this->getRequest();

    	$oEm = $this->get('doctrine')->getManager();
        $oTF = $this->get('traitement.fiches');
    	
    	$oFiche = new Fiche;

        $oForm = $this->createForm(new FicheType(), $oFiche);

        if ( 
        		$oSession->has('Fiche') && // La fiche existe en session
        		file_exists($oTF->getFullPath("Process") . '/' . $oSession->get('Fiche')) && // La fiche existe dans le dossier Process
        		$oSession->get('IP') === $oRequest->server->get("REMOTE_ADDR") // La même IP que l'opératrice qui traitait la fiche
        	)
        {
        	$sFiche = $oTF->fetchFileProcessed($oSession->get('Fiche'));
        }
        else $sFiche = $oTF->fetchFile();
	 

        // Envoie du Form
        if ($oRequest->getMethod() === 'POST' )  
        {
        	$oForm->bind($oRequest);

	        if ($oForm->isValid()) 
	        {
				$oFiche->setUser($this->get('security.context')->getToken()->getUser());
				// On le persiste
				$oEm->persist($oFiche);
	        	
				// On déclenche l'enregistrement
				$oEm->flush();

				$oTF->endProcess($oSession->get('Fiche'), $oFiche->getFacture());

				$oSession->getFlashBag()->add('fiche_enregistre', 'Fiche enregistrée avec succés');

				return $this->redirect($this->generateUrl('scan_butagaz_homepage'));

	        }
    	}



    	return $this->render('ScanButagazBundle:Default:index.html.twig', array(	      	
	      	'form'         => $oForm->createView(),
	      	'mFiche'	   => $sFiche
	    ));
    }

    public function modifierAction(Request $oRequest)
    {
        $iFicheRecherche = $oRequest->request->get('fiche_recherche');
    	$_oFiche = $oRequest->request->get('scan_butagazbundle_fichetype');
        $oSession = $this->get('session');
        $oRequest = $this->getRequest();
        $oEm = $this->get('doctrine')->getManager();
	    $oTF = $this->get('traitement.fiches');

        if ($oRequest->getMethod() === 'POST' )
        {
            if (strlen($iFicheRecherche) > 3)
        	{
        		$oFiche = $oEm->getRepository('ScanButagazBundle:Fiche')->findOneByFacture($iFicheRecherche);
        		$sFiche = $oTF->checkifExists($iFicheRecherche);

        		if (!is_object($oFiche) || !$sFiche )
        		{
        			$oSession->getFlashBag()->add('recherche_image_introuvable', 'Recherche introuvable');
        			return $this->redirect($this->generateUrl('scan_butagaz_homepage'));
        		}
                $oSession->set('id_fiche', $oFiche->getId());
                $oSession->getFlashBag()->add('recherche_trouver', 'Fiche rappatriée avec succée.');

    	        $oForm = $this->createForm(new FicheType(), $oFiche);

                return $this->render('ScanButagazBundle:Default:index.html.twig', array(            
                    'form'         => $oForm->createView(),
                    'mFiche'       => $sFiche
                ));

        	}
            else if ( is_array($_oFiche) )
            {
                $oFiche = $oEm->getRepository('ScanButagazBundle:Fiche')->find($oSession->get('id_fiche'));
                $oForm = $this->createForm(new FicheType(), $oFiche);
                $oForm->bind($oRequest);
                $oEm->persist($oFiche);
                $oSession->remove('id_fiche');
                // On déclenche l'enregistrement
                $oEm->flush();

                $oSession->getFlashBag()->add('fiche_modifier', 'Fiche modifiée avec succée.');

                return $this->redirect($this->generateUrl('scan_butagaz_homepage'));
            }
        }
        


        return $this->redirect($this->generateUrl('scan_butagaz_homepage'));

        


    	
    }

}
