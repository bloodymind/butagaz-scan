<?php

/* ScanButagazBundle::LayoutScanBundle.html.twig */
class __TwigTemplate_c7209229c89bde33bdec653c8463ae4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html lang=\"fr\">
<head>
\t<meta charset=\"utf-8\">
    <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
\t<meta name=\"HandheldFriendly\" content=\"True\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />
    <meta name=\"description\" content=\"Scan Butagaz\" />
    <meta name=\"content\" content=\"Scan Butagaz\" />
    <link rel=\"shortcut icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/img/favicon.png"), "html", null, true);
        echo "\" />
\t";
        // line 11
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 15
        echo "\t
    ";
        // line 16
        $this->displayBlock('javascripts', $context, $blocks);
        // line 22
        echo "
</head>
<body>
    ";
        // line 25
        $this->displayBlock('header', $context, $blocks);
        // line 34
        echo "
    ";
        // line 35
        $this->displayBlock('body', $context, $blocks);
        // line 38
        echo "
    ";
        // line 39
        $this->displayBlock('footer', $context, $blocks);
        // line 48
        echo "
</body>
</html>";
    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        echo " Scan Butagaz - ";
    }

    // line 11
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 12
        echo "\t<link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/css/bootstrap.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\" />
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/css/master.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"screen\" />
\t";
    }

    // line 16
    public function block_javascripts($context, array $blocks = array())
    {
        // line 17
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/js/master.js"), "html", null, true);
        echo "\"></script>
\t";
    }

    // line 25
    public function block_header($context, array $blocks = array())
    {
        // line 26
        echo "    <!--header-->
\t<header>
\t\t<div class=\"wrap clearfix\">

\t\t</div>
\t</header>
\t<!--//header-->
    ";
    }

    // line 35
    public function block_body($context, array $blocks = array())
    {
        // line 36
        echo "
    ";
    }

    // line 39
    public function block_footer($context, array $blocks = array())
    {
        // line 40
        echo "    \t<!--footer-->
\t<footer>
\t\t<div class=\"wrap clearfix\">
\t\t\t
\t\t</div>
\t</footer>
\t<!--//footer-->\t
    ";
    }

    public function getTemplateName()
    {
        return "ScanButagazBundle::LayoutScanBundle.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 40,  137 => 39,  132 => 36,  129 => 35,  118 => 26,  115 => 25,  109 => 20,  105 => 19,  101 => 18,  96 => 17,  93 => 16,  87 => 13,  79 => 11,  73 => 5,  67 => 48,  65 => 39,  62 => 38,  60 => 35,  57 => 34,  55 => 25,  50 => 22,  48 => 16,  45 => 15,  43 => 11,  39 => 10,  25 => 1,  106 => 46,  103 => 45,  82 => 12,  77 => 24,  70 => 19,  68 => 18,  64 => 17,  58 => 14,  54 => 12,  51 => 11,  44 => 8,  41 => 7,  34 => 4,  31 => 5,);
    }
}
