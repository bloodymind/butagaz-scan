<?php

namespace Scan\ButagazBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// Response
use Symfony\Component\HttpFoundation\Response;
// Security
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContext;

// Instance
use Scan\ButagazBundle\Entity\User;
use Scan\ButagazBundle\Form\UserType;

class AuthentificationController extends Controller
{

    /**
    * Methode        : Connexion 
    * Param          : NULL
    * But            : securisé l'enthification , voir app/config/security.yml
    * Envoie         : NULL
    */
    public function connexionAction() 
    {
	    $oRequest = $this->getRequest();
	    $oSession = $oRequest->getSession();
	 
	    // On vérifie s'il y a des erreurs d'une précédent soumission du formulaire
	    if ($oRequest->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) 
	    {
	    	$sError = $oRequest->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
	    } 
	    else 
	    {
		    $sError = $oSession->get(SecurityContext::AUTHENTICATION_ERROR);
		    $oSession->remove(SecurityContext::AUTHENTICATION_ERROR);
	    }

	 	// le message d'erreur $sError je le prend pas en charge sur TWIG template , par ce qu'il peut contenir une requete sql comme erreur !
	    return $this->render('ScanButagazBundle:Authentification:connexion.html.twig', array(
	      	// Valeur du précédent nom d'utilisateur rentré par l'internaute
	      	'derniere_username' => $oSession->get(SecurityContext::LAST_USERNAME),
	      	'erreur'         => $sError
	    ));
    }

    /**
    * Methode        : Inscription 
    * Param          : NULL
    * But            : inscription des Utilisateur normaux
    * Envoie         : NULL
    */
    public function inscriptionAction() 
    {
		$oEncodeFactory = $this->container->get('security.encoder_factory');
		// Le nom d'utilisateur et le mot de passe sont identiques
		
		$oUser = new User;
        $oForm = $this->createForm(new UserType(), $oUser);
        $oRequest = $this->getRequest();
        $oForm->bind($oRequest);
        // Envoie du Form
        if ($oRequest->getMethod() === 'POST' )  
        {
	        if ($oForm->isValid()) 
	        {
	            $oEm = $this->get('doctrine')->getEntityManager();
				// Encoder le Mot de passe 
				$oEncoder = $oEncodeFactory->getEncoder( $oUser );
				// Set le nouveau mot de passe avec le Salt
				$oUser->setPassword($oEncoder->encodePassword( $oUser->getPassword() , $oUser->getSalt()));
				// Un Simple Utilisateur a un Role USER
				$oUser->setRoles(array( $oUser->getRoles() ));
				// On le persiste
				$oEm->persist($oUser);
	        	
				// On déclenche l'enregistrement
				$oEm->flush();
				// Redirect to Login Page to Show the seccess message
				$this->get('session')->getFlashBag()->add('inscription_terminer', true );
				return $this->redirect( $this->generateUrl("scan_butagaz_inscription") );
	        }
    	}

    	return $this->render('ScanButagazBundle:Authentification:inscription.html.twig', array(	      	
	      	'form'         => $oForm->createView(),
	    ));
    }
}
