<?php

namespace Scan\ButagazBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('civilite' , 'choice' , array("choices" => array( "Mlle" => "Mlle",
                                                                    "Mme"  => "Mme",
                                                                    "M."   => "M.")
                                                , "multiple" => false , "expanded" => false))
            ->add('username' , 'text' )
            ->add('nom' , 'text' , array("required" => false))
            ->add('prenom' , 'text', array("required" => false))
            ->add('email' , 'email')
            ->add('password', 'password')
            ->add('adresse', 'textarea', array("required" => false))
            ->add('code_postal' , 'text', array("required" => false))
            ->add('tel', 'text', array("required" => false))
            ->add('roles' , 'choice' , array("choices" => array( "ROLE_USER" => "Opératrice",
                                                                 "ROLE_ADMIN"  => "Cp")
                                                , "multiple" => false , "expanded" => false))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Scan\ButagazBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'scan_butagazbundle_usertype';
    }
}
