<?php

/* ScanButagazBundle:Default:index.html.twig */
class __TwigTemplate_699110ea6c463d60a21593015a520ea4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("ScanButagazBundle::LayoutScanBundle.html.twig");

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'body' => array($this, 'block_body'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "ScanButagazBundle::LayoutScanBundle.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " - Page de d'acceuil
";
    }

    // line 7
    public function block_header($context, array $blocks = array())
    {
        // line 8
        echo " \t";
        $this->displayParentBlock("header", $context, $blocks);
        echo "
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "
<div class=\"container-fluid\">
\t<div class=\"row-fluid saisie_fiche\">
\t\t<div class=\"logo\">
\t\t\t<img src=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/scanbutagaz/img/logo.gif"), "html", null, true);
        echo "\" title=\"Neo-Data\"/>
\t\t\t<h2>Inscription</h2>
\t\t</div>
\t\t<div class=\"formulaire\" >
\t\t\t<div class=\"container-fluid wrapper\" >
\t\t  \t\t<form id=\"inscription\" class=\"form-signin\" method=\"post\" autocomplete=\"off\" accept-charset=\"utf-8\" ";
        // line 21
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'enctype');
        echo ">
\t\t\t  \t\t";
        // line 22
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flashbag"), "get", array(0 => "inscription_terminer"), "method")) {
            // line 23
            echo "\t\t\t\t\t    <div class=\"alert alert-success\">
\t\t\t\t\t    \t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
\t\t\t\t\t\t\t<p>Félicitation ,<br /> Votre compte a été créer.</p>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        // line 28
        echo "\t\t\t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 29
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "facture"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Contrat :"));
        echo "
\t\t  \t\t\t\t";
        // line 30
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "facture"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 31
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "facture"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t                ";
        // line 34
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "civilite"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Civilité* :"));
        echo "
\t\t                ";
        // line 35
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "civilite"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 36
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "civilite"), 'errors');
        echo "
\t\t            </div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 39
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "nom"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Nom* :"));
        echo "
\t\t  \t\t\t\t";
        // line 40
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "nom"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 41
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "nom"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 44
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "prenom"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Prénom* :"));
        echo "
\t\t  \t\t\t\t";
        // line 45
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "prenom"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 46
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "prenom"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 49
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "adresse"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Adresse :"));
        echo "
\t\t  \t\t\t\t";
        // line 50
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "adresse"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 51
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "adresse"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 54
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "codePostal"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Code postal :"));
        echo "
\t\t  \t\t\t\t";
        // line 55
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "codePostal"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 56
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "codePostal"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 59
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "tel"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Tél :"));
        echo "
\t\t  \t\t\t\t";
        // line 60
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "tel"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 61
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "tel"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 64
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "ville"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Ville :"));
        echo "
\t\t  \t\t\t\t";
        // line 65
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "ville"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 66
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "ville"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 69
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "dateConsignation"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Date de consignation* :"));
        echo "
\t\t  \t\t\t\t";
        // line 70
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "dateConsignation"));
        foreach ($context['_seq'] as $context["_key"] => $context["part_date"]) {
            // line 71
            echo "\t\t  \t\t\t\t\t";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "part_date"), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t  \t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['part_date'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "\t\t                ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "dateConsignation"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 76
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "dateDeconsignation"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Date de déconsignation* :"));
        echo "
\t\t  \t\t\t\t";
        // line 77
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "form"), "dateDeconsignation"));
        foreach ($context['_seq'] as $context["_key"] => $context["part_date"]) {
            // line 78
            echo "\t\t  \t\t\t\t\t";
            echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "part_date"), 'widget', array("attr" => array("class" => "form-control")));
            echo "
\t\t  \t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['part_date'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "\t\t                ";
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "dateDeconsignation"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 83
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "enseigne"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Enseigne* :"));
        echo "
\t\t  \t\t\t\t";
        // line 84
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "enseigne"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 85
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "enseigne"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t<div class=\"item\">
\t\t  \t\t\t\t";
        // line 88
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "numeroIdentite"), 'label', array("label_attr" => array("class" => "form_label grid_5 first"), "label" => "Numéro d'identité* :"));
        echo "
\t\t  \t\t\t\t";
        // line 89
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "numeroIdentite"), 'widget', array("attr" => array("class" => "form-control")));
        echo "
\t\t                ";
        // line 90
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getAttribute($this->getContext($context, "form"), "numeroIdentite"), 'errors');
        echo "
\t\t  \t\t\t</div>
\t\t  \t\t\t";
        // line 92
        echo $this->env->getExtension('form')->renderer->searchAndRenderBlock($this->getContext($context, "form"), 'rest');
        echo "
\t\t\t\t\t<button type=\"submit\" class=\"btn btn-default\">Fiche suivante</button>
\t\t\t\t</form>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"image_fiche\">
\t\t\t<a href=\"";
        // line 98
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fiches/2.jpg"), "html", null, true);
        echo "\"><img src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("fiches/2.jpg"), "html", null, true);
        echo "\" title=\"\" alt=\"\" class=\"fiche_principale\" /></a>
\t\t</div>
\t</div>
</div>
";
    }

    // line 104
    public function block_footer($context, array $blocks = array())
    {
        // line 105
        echo "\t";
        $this->displayParentBlock("footer", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "ScanButagazBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  294 => 105,  291 => 104,  280 => 98,  271 => 92,  266 => 90,  262 => 89,  258 => 88,  252 => 85,  248 => 84,  244 => 83,  237 => 80,  228 => 78,  224 => 77,  220 => 76,  213 => 73,  204 => 71,  200 => 70,  196 => 69,  190 => 66,  186 => 65,  182 => 64,  176 => 61,  172 => 60,  168 => 59,  162 => 56,  158 => 55,  154 => 54,  148 => 51,  144 => 50,  140 => 49,  134 => 46,  130 => 45,  126 => 44,  120 => 41,  116 => 40,  112 => 39,  106 => 36,  102 => 35,  98 => 34,  92 => 31,  88 => 30,  84 => 29,  81 => 28,  74 => 23,  72 => 22,  68 => 21,  60 => 16,  54 => 12,  51 => 11,  44 => 8,  41 => 7,  34 => 4,  31 => 3,);
    }
}
