<?php

namespace Scan\ButagazBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FicheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('facture', 'text', array('required' => true, 'max_length' => 8))
            ->add('civilite' , 'choice' , array(
                "choices" => array( 
                    "Mlle",
                    "Mme",
                    "M."), 
                "multiple"      => false , 
                "expanded"      => false, 
                "empty_value"   => '',
                "required"      => true)
            )
            ->add('nom', 'text', array('required' => true, 'max_length' => 22))
            ->add('prenom', 'text', array('required' => true, 'max_length' => 22))
            ->add('adresse', 'textarea', array('required' => true))
            ->add('codePostal', 'text', array('required' => true, 'max_length' => 6))
            ->add('ville', 'text', array('required' => true, 'max_length' => 22))
            ->add('tel', 'text', array('required' => true, 'max_length' => 10))
            /*->add('dateConsignation', 'date', array(
                'required'      => true, 
                'widget'        => 'single_text',
                'max_length'    => 10
                )
            )*/
            ->add('dateConsignation', 'text', array(
                'required' => true,
                'max_length' => 10
                )
            )
            ->add('dateDeconsignation', 'text', array(
                'required' => true,
                'max_length' => 10
                )
            )
            ->add('enseigne', 'text', array('required' => true, 'max_length' => 20))
            ->add('numeroIdentite', 'text', array('required' => true, 'max_length' => 18))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Scan\ButagazBundle\Entity\Fiche'
        ));
    }

    public function getName()
    {
        return 'scan_butagazbundle_fichetype';
    }
}
