<?php
namespace Scan\ButagazBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

// UniqueEntity
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 * @ORM\Table(name="_users")
 * @ORM\Entity(repositoryClass="Scan\ButagazBundle\Entity\Repositories\UserRepository")
 * @UniqueEntity(fields={"email"} , message ="E-mail déjà existant !")
 * @UniqueEntity(fields={"username"} , message ="Nom d'Utilisateur déjà existant !")
 */
class User implements UserInterface , \Serializable
{
    /**
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;
 
  /**
   * @ORM\Column(name="civilite", type="string", length=10 , nullable=true)
   */
  private $civilite;

  /**
   * @ORM\Column(name="username", type="string", length=255, unique=true )
   */
  private $username;
    
  /**
   * @var string $nom
   * @Assert\Regex(pattern="/^([a-zA-Z ]*)$/" , message="Veuillez respecter le format requis.")
   * @ORM\Column(name="nom", type="string", length=100 , nullable=true)
   */
  private $nom;

  /**
   * @var string $prenom
   * @Assert\Regex(pattern="/^([a-zA-Z ]*)$/" , message="Veuillez respecter le format requis.")
   * @ORM\Column(name="prenom", type="string", length=100 , nullable=true)
   */
  private $prenom;

  /**
   * @ORM\Column(name="email", type="string", length=255 , nullable=true , unique=true)
   * @Assert\Email(message="L'email semble incorrecte!")
   */
  private $email;

  /**
   * @ORM\Column(name="password", type="string", length=255 , nullable=true)
   */
  private $password;

  /**
   * @ORM\Column(name="salt", type="string", length=255 , nullable=true)
   */
  private $salt;

  /**
   * @ORM\Column(name="adresse", type="string", length=255 , nullable=true)
   */
  private $adresse;

  /**
   * @var string $code_postal
   * @Assert\Regex(pattern="/^([0-9]*)$/" , message="Veuillez respecter le format requis.")
   * @ORM\Column(name="code_postal", type="string", length=6 , nullable=true)
   */
  private $code_postal;

  /**
   * @var string $tel
   * @Assert\Regex(pattern="/^([0-9]*)$/" , message="Veuillez respecter le format requis.")
   * @ORM\Column(name="tel", type="string", length=15 , nullable=true)
   */
  private $tel;

  /**
   * @var string $ip
   *
   * @ORM\Column(name="ip", type="string", length=15 , nullable=true)
   */
  private $ip;

  /**
   * @var \DateTime $date_inscription
   *
   * @ORM\Column(name="date_inscription", type="date")
   */
  private $date_inscription;
 
  /**
   * @ORM\Column(name="roles", type="array")
   */
  private $roles;

  public function __sleep()
  {
      return array('id');
  }
 
  public function __construct()
  {
    //$this->roles = array();
    $this->salt  = md5(uniqid(null, true)); 
    $this->setIp( @$_SERVER['REMOTE_ADDR'] );
    $this->setDateInscription( new \DateTime );
  }
 
  public function getId()
  {
    return $this->id;
  }
 
  public function setUsername($username)
  {
    $this->username = strtolower( ucfirst( $username ) );
    return $this;
  }
 
  public function getUsername()
  {
    return $this->username;
  }
 
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }
 
  public function getPassword()
  {
    return $this->password;
  }
 
  public function setSalt($salt)
  {
    $this->salt = $salt;
    return $this;
  }
 
  public function getSalt()
  {
    return $this->salt;
  }
 
  public function setRoles( $roles)
  {
    $this->roles = $roles;
    return $this;
  }
 
  public function getRoles()
  {
    return $this->roles;
  }
 
  public function eraseCredentials()
  {

  }
  public function isEqualTo(UserInterface $user)
  {
    return $this->username === $user->getUsername();
  }

    /**
     * Set nom
     *
     * @param string $nom
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = strtolower( ucfirst( $nom ) );
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = strtolower( ucfirst( $prenom ) );
    
        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set civilite
     *
     * @param string $civilite
     * @return User
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;
    
        return $this;
    }

    /**
     * Get civilite
     *
     * @return string 
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = strtolower( ucfirst( $adresse ) );
    
        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set code_postal
     *
     * @param string $codePostal
     * @return User
     */
    public function setCodePostal($codePostal)
    {
        $this->code_postal = $codePostal;
    
        return $this;
    }

    /**
     * Get code_postal
     *
     * @return string 
     */
    public function getCodePostal()
    {
        return $this->code_postal;
    }

    /**
     * Set tel
     *
     * @param string $tel
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    
        return $this;
    }

    /**
     * Get tel
     *
     * @return string 
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return User
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string 
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set date_inscription
     *
     * @param \DateTime $dateInscription
     * @return User
     */
    public function setDateInscription($dateInscription)
    {
        $this->date_inscription = $dateInscription;
    
        return $this;
    }

    /**
     * Get date_inscription
     *
     * @return \DateTime 
     */
    public function getDateInscription()
    {
        return $this->date_inscription;
    }

    public function serialize(){
      return serialize(array(
        $this->id,
        $this->password,
        $this->username
      ));
    }
    
    public function unserialize($serialized){
      list(
        $this->id,
        $this->password,
        $this->username
        ) = unserialize($serialized);
    }

}