<?php

namespace Scan\ButagazBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use Scan\ButagazBundle\Entity\Fiche;
use Scan\ButagazBundle\Form\FicheType;

class DefaultController extends Controller
{
    public function indexAction(Request $oRequest)
    {
    	$iFicheRecherche = $oRequest->request->get('fiche_recherche');
        $oSession = $this->get('session');
        $oRequest = $this->getRequest();

    	$oEm = $this->get('doctrine')->getManager();
        $oTF = $this->get('traitement.fiches');

        $sFiche = false;

    	if (strlen($iFicheRecherche) > 3)
    	{
    		$oFiche = $oEm->getRepository('ScanButagazBundle:Fiche')->findOneByFacture($iFicheRecherche);
    		$sFiche = $oTF->checkifExists($iFicheRecherche);
    		if ( !$sFiche ) $oSession->getFlashBag()->add('recherche_image_introuvable', 'Recherche introuvable (Image)');
    	}
    	else $oFiche = new Fiche;

        $oForm = $this->createForm(new FicheType(), $oFiche);

        /*echo '<pre>', print_r($oFiche, true), '</pre>';
        echo '<pre>', print_r(new Fiche, true), '</pre>';
        die();*/

        if ( !$sFiche )
        {
	        if ( 
	        		$oSession->has('Fiche') && // La fiche existe en session
	        		file_exists($oTF->getFullPath("Process") . '/' . $oSession->get('Fiche')) && // La fiche existe dans le dossier Process
	        		$oSession->get('IP') === $oRequest->server->get("REMOTE_ADDR") // La même IP que l'opératrice qui traitait la fiche
	        	)
	        {
	        	$sFiche = $oTF->fetchFileProcessed($oSession->get('Fiche'));
	        }
	        else $sFiche = $oTF->fetchFile();
	    }

        // Envoie du Form
        if ($oRequest->getMethod() === 'POST' )  
        {
        	$oForm->bind($oRequest);

	        if ($oForm->isValid()) 
	        {
				$oFiche->setUser($this->get('security.context')->getToken()->getUser());
				/*$oFiche->setDateConsignation($oFiche->getDateConsignation()->format('d-m-Y'));
				$oFiche->setDateDeconsignation($oFiche->getDateDeconsignation()->format('d-m-Y'));*/
				// On le persiste
				$oEm->persist($oFiche);
	        	
				// On déclenche l'enregistrement
				$oEm->flush();

				$oTF->endProcess($oSession->get('Fiche'), $oFiche->getFacture());

				$oSession->getFlashBag()->add('fiche_enregistre', 'Fiche enregistrée avec succés');

				return $this->redirect($this->generateUrl('scan_butagaz_homepage'));

	        }
    	}



    	return $this->render('ScanButagazBundle:Default:index.html.twig', array(	      	
	      	'form'         => $oForm->createView(),
	      	'mFiche'	   => $sFiche
	    ));
    }

    public function modifierAction(Request $oRequest)
    {
    	$iFicheRecherche = $oRequest->request->get('fiche_recherche');
        $oSession = $this->get('session');
        $oRequest = $this->getRequest();

        if (strlen($iFicheRecherche) > 3)
    	{
    		$oFiche = $oEm->getRepository('ScanButagazBundle:Fiche')->findOneByFacture($iFicheRecherche);
    		$sFiche = $oTF->checkifExists($iFicheRecherche);
    		if ( !$sFiche ) $oSession->getFlashBag()->add('recherche_image_introuvable', 'Recherche introuvable (Image)');
    	}
    }

}
